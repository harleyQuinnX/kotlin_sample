package com.example.harleyquinn.challenge03

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mission1.setOnClickListener{
            startActivity(Intent(this, Mission1Activity::class.java))
        }

        mission2.setOnClickListener {
            startActivity(Intent(this, Mission2Activity::class.java))
        }
    }
}
