package com.example.harleyquinn.challenge03

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.*

/**
 * Created by harleyquinn on 2017. 12. 6..
 */
class SettingFragment : PreferenceFragment() {

    val preference = lazy { PreferenceManager.getDefaultSharedPreferences(activity) }

    val networkType = lazy { findPreference("network_type") as ListPreference }
//    val apnScreen = lazy { findPreference("apn") as PreferenceScreen }
    val networkVendor = lazy { findPreference("network_vendor") as Preference }

    val listener = SharedPreferences.OnSharedPreferenceChangeListener { s: SharedPreferences, key: String ->
        when(key) {
            "network_type" -> {
                networkType.value.summary = preference.value.getString("network_type", "LTE(권장)")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.permission)

//        networkVendor.value.setOnPreferenceClickListener {
//            startActivity(Intent(activity, EmptyActivity::class.java))
//            true
//        }

        preference.value.registerOnSharedPreferenceChangeListener(listener)
    }
}