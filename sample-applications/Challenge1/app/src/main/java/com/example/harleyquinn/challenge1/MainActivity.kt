package com.example.harleyquinn.challenge1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.harleyquinn.challenge2.BrunchActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        confirm_btn.setOnClickListener {
            Toast.makeText(this, "Ok button Click!", Toast.LENGTH_SHORT).show()
        }

        setting_btn.setOnClickListener {
            startActivity(Intent(this, BrunchActivity::class.java ))
        }
    }
}
