package com.example.harleyquinn.challenge2

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.example.harleyquinn.challenge1.R
import kotlinx.android.synthetic.main.activity_brunch.*

class BrunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brunch)

        close_btn.setOnClickListener {
            startActivity(Intent(this, ContactActivity::class.java))
        }

    }

    override fun onBackPressed() {

        val alertDialog = AlertDialog.Builder(this)

        alertDialog.setTitle("작성중인 내용을 저장하지 않고 나가시겠습니까?")

        alertDialog.setPositiveButton("확인", { dialog, which ->
            finish()
        })

        alertDialog.setNegativeButton("취소", null)

        alertDialog.show()
    }
}
