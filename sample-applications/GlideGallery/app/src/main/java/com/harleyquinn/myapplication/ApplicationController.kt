package com.harleyquinn.myapplication

import android.app.Activity
import android.app.Application
import android.content.pm.ApplicationInfo
import com.kakao.auth.KakaoAdapter
import com.kakao.auth.KakaoSDK
import com.kakao.usermgmt.response.model.UserProfile

/**
 * Created by juhee on 2017. 11. 20..
 */
class ApplicationController : Application() {

    companion object {
        var instance: ApplicationController? = null
        var currentActivity : Activity? = null
        lateinit var loginUserProfile : UserProfile
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        KakaoSDK.init(KakaoSDKAdapter())
    }

    override fun onTerminate() {
        super.onTerminate()
        instance = null
    }
}