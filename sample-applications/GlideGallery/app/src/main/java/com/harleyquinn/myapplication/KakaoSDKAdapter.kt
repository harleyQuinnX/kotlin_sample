package com.harleyquinn.myapplication

import android.app.Activity
import android.content.Context
import com.kakao.auth.*

/**
 * Created by juhee on 2017. 11. 20..
 */
class KakaoSDKAdapter : KakaoAdapter() {
    override fun getApplicationConfig(): IApplicationConfig {
         return object : IApplicationConfig {
             override fun getTopActivity(): Activity? {
                 return ApplicationController.currentActivity
             }

             override fun getApplicationContext(): Context? {
                 return ApplicationController.instance
             }

        }
    }

    override fun getSessionConfig(): ISessionConfig {
        return object : ISessionConfig {
            override fun getAuthTypes(): Array<AuthType> {
                return arrayOf(AuthType.KAKAO_LOGIN_ALL)
            }

            override fun isSaveFormData(): Boolean = true

            override fun isUsingWebviewTimer(): Boolean = false

            override fun getApprovalType(): ApprovalType = ApprovalType.INDIVIDUAL
        }
    }
}