package com.harleyquinn.myapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.kakao.auth.ErrorCode
import com.kakao.network.ErrorResult
import com.kakao.usermgmt.UserManagement.requestMe
import com.kakao.usermgmt.callback.MeResponseCallback
import com.kakao.usermgmt.response.model.UserProfile
import com.kakao.util.helper.log.Logger

class KakaoSignUpActivity : AppCompatActivity() {

    companion object {
        val TAG = "KakaoSignUpActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(TAG, "onCreate")
        requestMe()
        Log.d(TAG, "afterRequest")
        setContentView(R.layout.activity_kakao_sign_up)
    }

    protected fun requestMe() : Unit {
//        var propertyKeys = listOf<String>("nickname")
//
//        Log.d(TAG, propertyKeys.toString())

        requestMe(object : MeResponseCallback() {
            override fun onFailure(errorResult: ErrorResult?) {
                Log.d(TAG, "onFailure")
                var message: String = "failure to get user info .msg = " + errorResult
                Logger.d(message)

                var result = ErrorCode.valueOf(errorResult!!.getErrorCode())

                when (result) {
                    ErrorCode.NOT_REGISTERED_PROPERTY_KEY_CODE -> {
                        Log.d(TAG, "적절하지 않은 attribute 값 호출")
                        finish()
                    }
                    ErrorCode.CLIENT_ERROR_CODE -> {
                        Log.d(TAG, "클라이언트 에러")
                        finish()
                    }
                    else -> {
                        Log.d(TAG, result.toString())
                        redirectLoginActivity()
                    }
                }
            }

            override fun onSessionClosed(errorResult: ErrorResult?) {
                Log.d(TAG, "onSessionClosed")
                redirectLoginActivity()
            }

            override fun onNotSignedUp() {
                Log.d(TAG, "onNotSignedUp")
                redirectLoginActivity()
            }

            override fun onSuccess(result: UserProfile) {
                Log.d(TAG, "onSuccess")
                Log.d(TAG, result.toString())
                ApplicationController.loginUserProfile = result
                redirectMainActivity()
            }
//        }, propertyKeys, false)
        })
    }

    protected fun redirectMainActivity() {
        Log.d(TAG, "redirectMainActivity")
        startActivity(Intent(this, MainActivity::class.java))
    }
    protected fun redirectLoginActivity() {
        Log.d(TAG, "redirectLoginActivity")
        val intent = Intent(this, LoginActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(intent)
        finish()
    }
}
