package com.harleyquinn.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userName.text = ApplicationController.loginUserProfile.nickname

        Glide.with(this).load(ApplicationController.loginUserProfile.thumbnailImagePath).into(thumbImage)

        Log.d(TAG, ApplicationController.loginUserProfile.toString())
    }


}
