package com.harleyquinn.myapplication.common

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Handler
import android.os.Looper
import com.kakao.util.helper.log.Logger

/**
 * Created by juhee on 2017. 11. 20..
 */
class WaitingDialog {

    companion object {
        val mainHandler = Handler(Looper.getMainLooper())
        val waitingDialogLock = Object()
        var waitingDialog: Dialog? = null

        fun showWaitingDialog(context: Context) {
            showWaitingDialog(context, false)
        }

        fun showWaitingDialog(context: Context, cancelable: Boolean) {
            showWaitingDialog(context, false, null)
        }

        fun showWaitingDialog(context: Context, cancelable: Boolean, listener: DialogInterface.OnCancelListener?) {
            cancelWaitingDialog()
            mainHandler.post(Runnable {
//                waitingDialog = Dialog(context, R.style.CustomProgressDialog)
//                waitingDialog!!.setContentView(R.layout.layout_waiting_dialog)
                waitingDialog!!.setCancelable(cancelable)

                if (listener != null) {
                    waitingDialog!!.setOnCancelListener(listener)
                }

                waitingDialog!!.show()
            })

        }

        fun cancelWaitingDialog() {
            mainHandler.post {
                try {
                    if (waitingDialog != null) {
                        waitingDialog!!.cancel()
                        waitingDialog = null
                    }
                } catch (e: Exception) {
                    Logger.d(e)
                }

            }
        }
    }
}