package com.harleyquinn.kotlinexam

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_event.*

class EventActivity : AppCompatActivity(), View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClick(v: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    internal lateinit var bellTextView: TextView
    internal lateinit var labelTextVeiw: TextView
    internal lateinit var repeatCheckView: CheckBox
    internal lateinit var vibrateCheckView: CheckBox
    internal lateinit var switchView: Switch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bellTextView = bell_name
        labelTextVeiw = label
        repeatCheckView = repeatCheck
        vibrateCheckView = vibrate
        switchView = onOff

    }


    private fun showToast(message: String) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        toast.show()
    }
}
