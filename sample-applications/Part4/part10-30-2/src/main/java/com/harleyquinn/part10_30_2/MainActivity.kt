package com.harleyquinn.part10_30_2

import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mediaRecoder = MediaRecorder()
        val dir = File(Environment.getExternalStorageDirectory().absolutePath+"/myApp")

        if (!dir.exists()) {
            dir.mkdir()
            Log.d("mkdir", "mkdir : ${dir.exists()}")
        }

        val file = File.createTempFile("RECORD-", ".3gp", dir)

        with(mediaRecoder) {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(file.absolutePath)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
        }

        record_start.setOnClickListener {
            mediaRecoder.prepare()
            mediaRecoder.start()
        }

        record_stop.setOnClickListener {
            mediaRecoder.stop()
            mediaRecoder.release()
        }


        play_start.setOnClickListener {
            val mediaPlayer = MediaPlayer()
            mediaPlayer.setDataSource(file.absolutePath)
            mediaPlayer.start()
        }

//        val fft:RealDoubleFFT

    }
}
