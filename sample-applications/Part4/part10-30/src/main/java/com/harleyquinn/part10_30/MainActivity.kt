package com.harleyquinn.part10_30

import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.widget.Button
import android.widget.MediaController
import android.widget.VideoView
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by juhee on 2018. 1. 1..
 */

class MainActivity : AppCompatActivity(), View.OnClickListener, SurfaceHolder.Callback {

    lateinit var soundMediaBtn:Button
    lateinit var soundPoolBtn:Button
    lateinit var videoMediaBtn:Button
    lateinit var videoViewBtn:Button

    lateinit var surfaceView:SurfaceView
    lateinit var videoView:VideoView

    private var mediaPlayer:MediaPlayer? = null
    private lateinit var holder:SurfaceHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("이게뭐람","이게뭐람")

        soundMediaBtn = lab1_sound_media
        soundMediaBtn.setOnClickListener(this)
        soundPoolBtn = lab1_sound_pool
        soundPoolBtn.setOnClickListener(this)
        videoMediaBtn = lab1_video_media
        videoMediaBtn.setOnClickListener(this)
        videoViewBtn = lab1_video_view
        videoViewBtn.setOnClickListener(this)
        surfaceView = lab1_surface
        videoView = lab1_video

        holder= surfaceView.holder
        holder.addCallback(this)
    }


    override fun onClick(v: View?) {

        if (mediaPlayer != null) {
            mediaPlayer!!.stop()
            mediaPlayer!!.release()
            mediaPlayer = null
        }

        when(v) {
            soundMediaBtn -> {
                mediaPlayer = MediaPlayer.create(this, R.raw.sound)
                mediaPlayer!!.start()
            }
            soundPoolBtn -> {
                val soundPool = SoundPool.Builder().setMaxStreams(10).build()
                soundPool.load(this, R.raw.sound, 1)
                soundPool.setOnLoadCompleteListener { soundPool, sampleId, _ ->
                    soundPool.play(sampleId, 1f, 1f, 0, 0, 1f)
                }
            }
            videoMediaBtn -> {
                mediaPlayer = MediaPlayer.create(this, R.raw.video)
                with(mediaPlayer!!) {
                    setDisplay(holder)
                    setAudioStreamType(AudioManager.STREAM_MUSIC)
                    start()
                }
            }
            videoViewBtn -> {
                val mediaController = MediaController(this)
                val uri = Uri.parse("android.resource://$packageName/${R.raw.video}")
                with(videoView) {
                    setVideoURI(uri)
                    setMediaController(mediaController)
                    requestFocus()
                    start()
                }
            }

        }

    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {

    }

    override fun surfaceCreated(holder: SurfaceHolder?) {

    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaPlayer != null) {
            mediaPlayer!!.release()
            mediaPlayer = null
        }
    }

}