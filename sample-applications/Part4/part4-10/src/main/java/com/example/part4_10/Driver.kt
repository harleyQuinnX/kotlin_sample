package com.example.part4_10

import android.content.Context
import android.support.v4.content.res.ResourcesCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.custom_item.view.*

/**
 * Created by harleyquinn on 2017. 12. 11..
 *
 */
class DriverVO(val type:String, val title:String, val date:String)

class DriveHolder(root:View){

    val imageView:ImageView  = root.custom_item_type_image
    val titleView:TextView= root.custom_item_title
    val dateView:TextView= root.custom_item_date
    val menuImageView:ImageView= root.custom_item_menu
}

class DriverAdapter(cxt:Context, private val resId:Int, private val datas:ArrayList<DriverVO>) : ArrayAdapter<DriverVO>(cxt, resId) {

    override fun getCount(): Int = datas.size

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val cnvView:View
        val holder:DriveHolder

        if(convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            cnvView = inflater.inflate(resId, null)
            holder = DriveHolder(cnvView)
            cnvView.tag = holder

        } else {
            holder = convertView.tag as DriveHolder
            cnvView = convertView
        }

        val vo = datas[position]

        holder.titleView.text = vo.title
        holder.dateView.text = vo.date
        

        val typeResource = when(vo.type) {
            "doc" -> R.drawable.ic_type_doc
            "file"-> R.drawable.ic_type_file
            "img" -> R.drawable.ic_type_image
            else  -> R.drawable.ic_type_file
        }

        holder.imageView.setImageDrawable(ResourcesCompat.getDrawable(context.resources, typeResource, null))

        holder.menuImageView.setOnClickListener{
            Toast.makeText(context, "${vo.title} menu Click", Toast.LENGTH_SHORT).show()
        }
        return cnvView
    }

}