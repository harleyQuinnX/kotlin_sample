package com.example.part4_10

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_lab10_2.*

class Lab10_2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab10_2)

        val listView = custom_listview

        val helper = DBHelper(this)
        val db = helper.writableDatabase
        val cursor = db.rawQuery("select * from tb_drive", null)

        val datas = arrayListOf<DriverVO>()
        while(cursor.moveToNext()){
            Log.d("AAAA", "몇번이니")
            datas.add(DriverVO(cursor.getString(3),cursor.getString(2),cursor.getString(1)))
        }
        db.close()
        cursor.close()

        listView.adapter = DriverAdapter(this, R.layout.custom_item, datas)
    }
}
