package com.example.part4_10

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemClickListener {
    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Toast.makeText(this, arrayDatas!![position], Toast.LENGTH_SHORT).show()
    }

    var arrayDatas: Array<String>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val arrayView = main_listview_array
        val simpleView = main_listview_simple
        val cursorView = main_listview_cursor

        arrayView.onItemClickListener = this

        arrayDatas = resources.getStringArray(R.array.location)
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayDatas)
        arrayView.adapter = arrayAdapter

        val simpleDatas = arrayListOf<HashMap<String, String>>()
        val helper = DBHelper(this)
        val db = helper.writableDatabase
        val cursor = db.rawQuery("select * from tb_data", null)

        while (cursor.moveToNext()) {
            val map = hashMapOf("name" to cursor.getString(1), "content" to cursor.getString(2))
            simpleDatas.add(map)
        }

        val simpleAdapter = SimpleAdapter(this, simpleDatas, android.R.layout.simple_list_item_2, arrayOf("name", "content"), arrayOf(android.R.id.text1, android.R.id.text2).toIntArray())
        simpleView.adapter = simpleAdapter

        val cursorAdapter:CursorAdapter = SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, arrayOf("name", "content"), arrayOf(android.R.id.text1, android.R.id.text2).toIntArray(), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        cursorView.adapter = cursorAdapter

    }

}
