package com.example.harleyquinn.part4

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.widget.SearchView
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_lab12_2.*

class MenuActivity : AppCompatActivity() {

    var searchView:SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        registerForContextMenu(imageView)
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        if(menu is MenuBuilder) menu.setOptionalIconsVisible(true)

        val menuItem = menu!!.findItem(R.id.menu_main_search)
        searchView = menuItem.actionView as SearchView
        searchView!!.queryHint = resources.getString(R.string.query_hint)
        searchView!!.setOnQueryTextListener(queryTextListener)

        return true
    }

    private val queryTextListener = object :SearchView.OnQueryTextListener {
        override fun onQueryTextChange(newText: String?): Boolean {
            return false
        }

        override fun onQueryTextSubmit(query: String?): Boolean {
            searchView!!.setQuery("", false)
            searchView!!.setIconifiedByDefault(true)
            Toast.makeText(this@MenuActivity, query, Toast.LENGTH_SHORT).show()
            return false

        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menu.add(0,0,0,"서버전송")
        menu.add(0,1,0, "보관함에 보관")
        menu.add(0, 2, 0, "삭제")
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            0 -> Toast.makeText(this@MenuActivity, item.title, Toast.LENGTH_SHORT).show()
            1 -> Toast.makeText(this@MenuActivity, item.title, Toast.LENGTH_SHORT).show()
            2 -> Toast.makeText(this@MenuActivity, item.title, Toast.LENGTH_SHORT).show()
        }
        return true
    }
}
