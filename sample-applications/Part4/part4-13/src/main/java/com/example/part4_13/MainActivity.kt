package com.example.part4_13

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnMyChangeListener {

    override fun onChange(value: Int) {
        when {
            value < 0 -> barView.setBackgroundColor(Color.RED)
            value < 30 -> barView.setBackgroundColor(Color.YELLOW)
            value < 60 -> barView.setBackgroundColor(Color.BLUE)
            else ->  barView.setBackgroundColor(Color.GREEN)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (customView as MinusPlusView).setOnMyChangeListener(this)


    }
}
