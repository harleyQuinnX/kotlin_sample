package com.example.part4_13

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

/**
 * Created by juhee on 2017-12-14.
 *
 */
class MinusPlusView(val ctx:Context, attrs:AttributeSet? = null, defStyleAttr:Int = 0) : View(ctx, attrs, defStyleAttr) {
    private val plusBitmap:Bitmap = BitmapFactory.decodeResource(ctx.resources, R.drawable.plus)
    private val minusBitmap:Bitmap  = BitmapFactory.decodeResource(ctx.resources, R.drawable.minus)
    private var value:Int = 0
    private val plusRectDst:Rect  = Rect(10, 10, 210, 210)
    private val minusRectDst:Rect  = Rect(400, 10, 600, 210)
    private var textcolor:Int = 0
    private val plusRectSource = Rect(0, 0, plusBitmap.width, plusBitmap.height)
    private val minusRectSource = Rect(0, 0, minusBitmap.width, minusBitmap.height)

    private val paint = Paint()

    private val listeners = arrayListOf<OnMyChangeListener>()
    init {
        if (attrs != null) {
            val a = ctx.obtainStyledAttributes(attrs, R.styleable.MyView)
            textcolor = a.getColor(R.styleable.MyView_customTextColor, Color.RED)
            a.recycle()
        }
        paint.textSize = 80f
        paint.color = textcolor
    }

    fun setOnMyChangeListener(listener: OnMyChangeListener) = listeners.add(listener)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)

        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        var width:Int = when(widthMode) {
            MeasureSpec.AT_MOST -> 700
            else -> widthSize
        }
        var height:Int = when(heightMode) {
            MeasureSpec.AT_MOST -> 250
            else -> heightSize
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x.toInt()
        val y = event.y.toInt()

        if (plusRectDst.contains(x, y) && event.action == MotionEvent.ACTION_DOWN) {
            value++
            invalidate()
            listeners.forEach { listener ->
                listener.onChange(value)
            }
            return true
        } else if (minusRectDst.contains(x,y) && event.action == MotionEvent.ACTION_DOWN) {
            value--
            invalidate()
            listeners.forEach { listener ->
                listener.onChange(value)
            }
            return true
        }
        return false
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawColor(Color.alpha((Color.CYAN)))
        canvas.drawBitmap(plusBitmap, plusRectSource, plusRectDst, null)
        canvas.drawText(value.toString(), 260f, 150f, paint)
        canvas.drawBitmap(minusBitmap, minusRectSource, minusRectDst, null)
    }

}