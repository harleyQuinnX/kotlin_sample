package com.example.part4_13

/**
 * Created by juhee on 2017-12-14.
 */
interface OnMyChangeListener {
    fun onChange(value:Int)
}