package com.example.part4_challenge1

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_call_log.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db = DBHelper(this).writableDatabase
        val cursor = db.rawQuery("select name, photo, date, phone from tb_calllog", null)
        val logList = arrayListOf<CallLog>()
        while(cursor.moveToNext()) {
            logList.add(CallLog(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3)))
        }
        list_view.adapter = ListAdapter(logList)

        val permission:Boolean = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED

        if (!permission) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CALL_PHONE), 200)

        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    data class CallLog(val name:String, val photo:String, val date:String, val phone:String)

    inner class ListAdapter( val items:List<CallLog>) : ArrayAdapter<String>(this, R.layout.item_call_log) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val item:CallLog = items[position]
            if (convertView == null) {

                val inflater =  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.item_call_log, null)
            } else {
                view = convertView
            }

            view.person_name.text = item.name
            view.person_date.text = item.date
            view.person_profile.setImageResource(R.drawable.hong)
            view.btn_call.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_CALL
                intent.data = Uri.parse("tel: ${item.phone}")
                startActivity(intent)
            }

            return view
        }

        override fun getCount() = items.size
    }
}
