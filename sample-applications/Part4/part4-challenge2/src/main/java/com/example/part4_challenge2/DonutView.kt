package com.example.part4_challenge2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.widget.ProgressBar

/**
 * Created by harleyquinn on 2017. 12. 19..
 */
class DonutView : ProgressBar{

    var borderColor  = Color.RED
    var borderWidth = 20f
    var mProgress = 180f
    var mWidth = 0
    var mHeight = 0
    var mSize = 0f
    var mTextSize = 0f
    var mText = ""

    constructor(context:Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    init {
        mSize = resources.getDimensionPixelSize(R.dimen.donut_size).toFloat()
        borderWidth = resources.getDimensionPixelSize(R.dimen.donut_stroke_size).toFloat()
        mTextSize = resources.getDimensionPixelSize(R.dimen.donut_textSize).toFloat()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawColor(Color.alpha(Color.CYAN))

        val rectF = RectF(15f, 15f, mSize-20, mSize-20)
        val paint = Paint()
        paint.color = Color.GRAY
        paint.strokeWidth = borderWidth
        canvas!!.drawArc(rectF, 0f, 360f, false, paint)
        paint.color = borderColor
        canvas!!.drawArc(rectF, -90f, mProgress, false, paint)

        paint.textSize = mTextSize

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        mWidth = MeasureSpec.getSize(widthMeasureSpec);
        mHeight = MeasureSpec.getSize(heightMeasureSpec)
    }
}