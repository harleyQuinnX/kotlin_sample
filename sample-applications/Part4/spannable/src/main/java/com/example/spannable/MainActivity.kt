package com.example.spannable

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.ScrollingMovementMethod
import android.text.style.ImageSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val spanView = spanView
        spanView.movementMethod = ScrollingMovementMethod()

        val str = "복수초 \n img \n 이른봄 설산에서 만나는 복수초는 모든 야생화 찍사들의 로망이 아닐까 싶다."

        val builder = SpannableStringBuilder(str)
        val imgStartIndex = str.indexOf("img")
        if (imgStartIndex > -1) {
            val endIndex = imgStartIndex + "img".length
            val dr:Drawable = ResourcesCompat.getDrawable(resources, R.drawable.img1, null)!!
            dr.setBounds(0, 0, dr.intrinsicWidth, dr.intrinsicHeight)
            val imgSpan = ImageSpan(dr)
            builder.setSpan(imgSpan, imgStartIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        val boldStartIndex = str.indexOf("복수초")
        if (boldStartIndex > -1) {
            val endIndx = boldStartIndex + "복수초".length
            val styleSpan = StyleSpan(Typeface.BOLD)
            val sizeSpan = RelativeSizeSpan(2.0f)
            builder.setSpan(styleSpan, boldStartIndex, endIndx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            builder.setSpan(sizeSpan, boldStartIndex, endIndx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        spanView.text = builder

        val htmlView2 = htmlView
        val html = "<font color='RED'>얼레지</font> <br/> <img src='img2' /> <br/> 곰배령에서 만난 봄꽃"
//        htmlView.text = Html.fromHtml(html, Html.ImageGetter { source: String? ->
//            if(source.equals("img2")) {
//                Log.d("ㅇㅇㅇㅇ", "이게모야")
//                val dr:Drawable = ResourcesCompat.getDrawable(resources, R.drawable.img1, null)!!
//                dr.setBounds(0, 0, dr.intrinsicWidth, dr.intrinsicHeight)
//                dr
//            }
//            null
//
//        }, null)
        htmlView2.text = Html.fromHtml(html, MyImageGetter(), null)
    }

    inner class MyImageGetter : Html.ImageGetter {
        override fun getDrawable(source: String?): Drawable? {
            if(source.equals("img2")) {
                Log.d("ㅇㅇㅇㅇ", "이게모야")
                val dr:Drawable = ResourcesCompat.getDrawable(resources, R.drawable.img2, null)!!
                dr.setBounds(0, 0, dr.intrinsicWidth, dr.intrinsicHeight)
                return dr
            }
            return null
        }

    }

}


