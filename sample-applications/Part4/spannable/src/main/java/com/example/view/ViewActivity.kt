package com.example.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import com.example.spannable.R
import kotlinx.android.synthetic.main.activity_view.*

class ViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view)

        val spinner = spinner
        val datas = resources.getStringArray(R.array.spinner_array)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, datas)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

        val autoTextView:AutoCompleteTextView = auto
        val autoDatas = resources.getStringArray(R.array.auto_array)
        val autoAdapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, autoDatas)

        autoTextView.setAdapter(autoAdapter)

        val progressBar = progress

        object :Thread() {
            override fun run() {
                for (i in 0..9) {
                    SystemClock.sleep(1000)
                    progressBar.incrementProgressBy(10)
                    progressBar.incrementSecondaryProgressBy(15)
                }
            }
        }.start()
    }
}
