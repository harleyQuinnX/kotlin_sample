package com.example.webview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.*
import android.widget.Toast
import com.example.spannable.R
import kotlinx.android.synthetic.main.activity_lab11_2.*
import java.time.LocalDate

class WebViewActivity : AppCompatActivity() {

    val lineBtn = lazy {btn_chart_line}
    val barBtn = lazy { btn_chart_bar }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab11_2)

            val webview = webview
            Log.d("WebView", "머디")
            lineBtn.value.setOnClickListener{
                webview.loadUrl("javascript:lineChart()")
            }
            barBtn.value.setOnClickListener{
                webview.loadUrl("javascript:barChart()")

            }

            val setting = webview.settings
            setting.javaScriptEnabled = true

            webview.loadUrl("file:///android_asset/test.html")
            webview.addJavascriptInterface(JavascriptTest(), "android")
            webview.webViewClient = object :WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url:String): Boolean {
                    Toast.makeText(this@WebViewActivity, url, Toast.LENGTH_SHORT).show()
                    return true
                }
            }
            webview.webChromeClient = object :WebChromeClient() {
                override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                    Toast.makeText(this@WebViewActivity, message, Toast.LENGTH_SHORT).show()
                    result!!.confirm()
                    return true
                }
            }


    }


    inner class JavascriptTest {
        @JavascriptInterface
        fun getChartData(): String {
            val buffer = StringBuffer()
            buffer.append("[")
            for(i in 0..13){
                buffer.append("[$i,${Math.sin(i.toDouble())}]")

                Log.d("JavascriptTest", "$i, ${Math.sin(i.toDouble())}")
                if( i < 13 ) buffer.append(",")
            }
            buffer.append("]")
            return buffer.toString()
        }
    }
}
