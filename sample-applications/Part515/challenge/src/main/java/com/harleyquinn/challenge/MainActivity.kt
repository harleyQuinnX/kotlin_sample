package com.harleyquinn.challenge

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileInputStream

class MainActivity : AppCompatActivity() {

    private var permission = false
    private val REQUEST_CODE_PICTURE = 0
    private val REQUEST_CODE_VIDEO = 1

    private val dirPath = Environment.getExternalStorageDirectory().absolutePath + "/challenge1"
    private lateinit var filePath : File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 200)
        }

        main_photo_icon.setOnClickListener { showDialog() }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 200 && grantResults.isNotEmpty()) {
            permission = true
            showDialog()
        }
    }

    private fun showDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("촬영")
        val listener = DialogInterface.OnClickListener { dialog, which ->


            when (which) {
                0 -> {
                    val dir = File(dirPath)
                    if (!dir.exists()) {
                        dir.mkdir()
                    }

                    filePath = File.createTempFile("IMG", ".jpg", dir)
                    if (!filePath.exists()) {
                        filePath.createNewFile()
                    }
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    val photoURI = FileProvider.getUriForFile(this, "${BuildConfig.APPLICATION_ID}.provider", filePath)

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(intent, REQUEST_CODE_PICTURE)
                }
                1 -> {
                    val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                    startActivityForResult(intent, REQUEST_CODE_VIDEO)
                }
            }
        }

        builder.setItems(R.array.main_dialog_list, listener)
        builder.create().show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_PICTURE -> {
                    val option = BitmapFactory.Options()
                    option.inJustDecodeBounds = true
                    val sampleSize : Int
                    var bitmap : Bitmap? = null

                    try {
                        val inputStream = FileInputStream(filePath)
                        bitmap = BitmapFactory.decodeStream(inputStream, null, option)
                        inputStream.close()

                    } catch (e : Exception) {
                        e.printStackTrace()
                    }

                    val height = option.outHeight
                    val width = option.outWidth

                    if (height > 120 || width > 260) {
                        val heightRatio = height / 120
                        val widthRatio = width / 260
                        sampleSize = if (heightRatio < widthRatio ) heightRatio else widthRatio
                        option.inSampleSize = sampleSize
                    }
//                    val bitmap = BitmapFactory.decodeFile(filePath.absolutePath, option)

                    val imageView = ImageView(this)
                    imageView.setImageBitmap(bitmap)
                    val layoutParam = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT)

                    layoutParam.resolveLayoutDirection(RelativeLayout.ALIGN_PARENT_RIGHT)
                    layoutParam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                    imageView.layoutParams = layoutParam
                    main_content.addView(imageView)
                }
                REQUEST_CODE_VIDEO -> {

                }
            }
        }
    }
}
