package com.harleyquinn.part5_16

import android.media.Image
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_lab16_2.*

class Lab16_2Activity : AppCompatActivity(), View.OnClickListener {

    var isFirst = true
    lateinit var startView:ImageView
    lateinit var  pauseView:ImageView
    lateinit var textView:TextView

    val asyncTask = MyAsyncTask()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab16_2)

        startView = main_startBtn
        pauseView = main_pauseBtn
        textView = main_textView

        startView.setOnClickListener(this)
        pauseView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            startView -> {
                if (isFirst) {
                    asyncTask.isRun = true
                    asyncTask.execute()
                    isFirst = false
                } else {
                    asyncTask.isRun = true
                }
            }
            pauseView -> {
                asyncTask.isRun = false
            }
        }
    }

    inner class MyAsyncTask : AsyncTask<Void, Int, String>() {

        var loopFlag = true
        var isRun = false

        override fun doInBackground(vararg params: Void?): String {
            var count = 10
            while (loopFlag) {
                SystemClock.sleep(1000)
                if (isRun) {
                    count--
                    publishProgress(count)
                    if (count == 0) {
                        loopFlag = false
                    }
                }
            }
            return "Finish!!!!"
        }

        override fun onProgressUpdate(vararg values: Int?) {
            textView.text = values[0].toString()
        }

        override fun onPostExecute(result: String?) {
            textView.text = result
        }

    }

    /*
    *
    *Lab16_2Activity.java
    *
    *
    *
    * */
}
