package com.harleyquinn.part5_16

import android.os.*
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_lab16_3.*
import java.util.*

class Lab16_3Activity : AppCompatActivity() {

    val oddDatas = arrayListOf<String>()
    val evenDatas = arrayListOf<String>()

    lateinit var oddAdapter : ArrayAdapter<String>
    lateinit var evenAdapter : ArrayAdapter<String>

    val handler:Handler = Handler()
    val oneThread = OneThread()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab16_3)

        val oddView = lab3_list_odd
        val evenView = lab3_list_even

        oddAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, oddDatas)
        evenAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, evenDatas)

        oddView.adapter = oddAdapter
        evenView.adapter = evenAdapter

        oneThread.start()

        val twoThread = TwoThread()
        twoThread.start()
    }

    inner class OneHandler : Handler() {
        override fun handleMessage(msg: Message) {
            SystemClock.sleep(1000)
            val data = msg.arg1
            Log.d("OneHandler", "what : ${msg.what}")

            when (msg.what) {
                0 -> {
                    handler.post {
                        evenDatas.add("even : $data")
                        evenAdapter.notifyDataSetChanged()
                    }
                }
                1 -> {
                    handler.post {
                        oddDatas.add("odd : $data")
                        oddAdapter.notifyDataSetChanged()
                    }
                }
            }
        }

    }

    inner class OneThread : Thread() {
        lateinit var oneHandler:Handler

        override fun run() {
            Looper.prepare()
            oneHandler = OneHandler()
            Looper.loop()
        }
    }

    inner class TwoThread : Thread() {

        override fun run() {
            val random = Random()
            for (i in 0 until 10) {
                SystemClock.sleep(100)
                val data = random.nextInt(10)
                val message = Message()
                message.what = if (data % 2 == 0) 0 else 1
                message.arg1 = data
                message.arg2 = i
                oneThread.oneHandler.sendMessage(message)
                Log.d("TwoThread", "what : ${message.what}")
            }
            Log.d("TwoThread", "two thread stop...")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        oneThread.oneHandler.looper.quit()
    }
}
