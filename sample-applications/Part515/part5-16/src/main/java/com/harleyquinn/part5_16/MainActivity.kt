package com.harleyquinn.part5_16

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var startView:ImageView
    private lateinit var endView:ImageView
    private lateinit var textView:TextView

    private var loopFlag = true
    private var isFirst = true
    private var isRun = false

    private val thread:MyThread = MyThread()
    private val handler:Handler = Handler { msg ->
        if (msg.what == 1) {
            textView.text = msg.arg1.toString()
        } else if (msg.what == 2) {
            textView.text = msg.obj.toString()
        }

        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startView = main_startBtn
        endView = main_pauseBtn
        textView = main_textView

        startView.setOnClickListener(this)
        endView.setOnClickListener(this)

    }
    override fun onClick(v: View?) {
        when (v) {
            startView -> {
                if (isFirst) {
                    isFirst = false
                    isRun = true
                    thread.start()
                } else {
                    isRun = true
                }
            }
            endView -> {
                isRun = false
            }
        }
    }



    inner class MyThread : Thread() {
        override fun run() {
            try {
                var count = 10
                while (loopFlag) {
                    Thread.sleep(1000)
                    if (isRun) {
                        count--
                        val message = Message()
                        message.what = 1
                        message.arg1 = count
                        handler.sendMessage(message)

                        if (count == 0) {
                            val msg = Message()
                            msg.what = 2
                            msg.obj = "Finish!!"
                            handler.sendMessage(msg)
                            loopFlag = false
                        }
                    }
                }
            } catch (e:Exception) {

            }
        }
    }
}
