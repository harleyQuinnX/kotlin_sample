package com.harleyquinn.part7_19

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_lab19_2.*

class Lab19_2Activity : AppCompatActivity(), View.OnClickListener {

    private val manager:NotificationManager by lazy { getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }
    private lateinit var builder:NotificationCompat.Builder

    private val basicBtn by lazy {lab2_basic}
    private val bigPictureBtn by lazy { lab2_bigpicture }
    private val bigTextBtn by lazy { lab2_bigtext }
    private val inboxBtn by lazy { lab2_inbox }
    private val progressBtn by lazy { lab2_progress }
    private val headsupBtn by lazy { lab2_headsup }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab19_2)

        basicBtn.setOnClickListener(this)
        bigPictureBtn.setOnClickListener(this)
        bigTextBtn.setOnClickListener(this)
        inboxBtn.setOnClickListener(this)
        progressBtn.setOnClickListener(this)
        headsupBtn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = "one channel"
            val channelName = "My one channel"
            val channelDescription = "My channel One Description"
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)

            channel.description = channelDescription
            manager.createNotificationChannel(channel)

            builder = NotificationCompat.Builder(this, channelId)

        } else {
            builder = NotificationCompat.Builder(this)
        }

        val intent = Intent(this, MainActivity::class.java)
        val pIntent = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val pIntent2 = PendingIntent.getBroadcast(this, 0, Intent(this, NotiReceiver::class.java), PendingIntent.FLAG_UPDATE_CURRENT)
        val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.noti_large)

        builder.apply {
            setSmallIcon(android.R.drawable.ic_notification_overlay)
            setContentTitle("Content Title")
            setContentText("Content Message")
            setAutoCancel(true)
            setContentIntent(pIntent)
            addAction(NotificationCompat.Action.Builder(android.R.drawable.ic_menu_share, "ACTION1", pIntent2).build())
            setLargeIcon(largeIcon)
        }
       when (v!!) {
           bigPictureBtn -> {
               val bigPicture = BitmapFactory.decodeResource(resources, R.drawable.noti_big)
               val bigStyle = android.support.v4.app.NotificationCompat.BigPictureStyle(builder)
               bigStyle.bigPicture(bigPicture)
               builder.setStyle(bigStyle)
           }
           bigTextBtn -> {
               val bigTextStyle = NotificationCompat.BigTextStyle(builder)
               bigTextStyle.setSummaryText("BigText Summary")
               bigTextStyle.bigText("동해물과 백두산이 마르고 닳도록 하느님이 보우하사 우리나라 만세")
               builder.setStyle(bigTextStyle)
           }
           inboxBtn -> {
               val style = NotificationCompat.InboxStyle(builder)
               style.addLine("Activity1")
               style.addLine("Activity2")
               style.addLine("Activity3")
               style.addLine("Activity4")
               style.setSummaryText("Android Component")
               builder.setStyle(style)
           }
           progressBtn -> {
               val runnable = Runnable {
                   for (i in 1..10) {
                       builder.setAutoCancel(false)
                       builder.setOngoing(true)
                       builder.setProgress(10, i, false)
                       manager.notify(222, builder.build())
                       if (i>=10) {
                           manager.cancel(222)
                       }
                       SystemClock.sleep(1000)
                   }
               }
               Thread(runnable).start()
           }

           headsupBtn -> {
               builder.setFullScreenIntent(pIntent, true)
           }
       }
        manager.notify(222, builder.build())
    }
}
