package com.harleyquinn.part7_19

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager

class MyReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action

        if (action == "android.intent.action.NEW_OUTGOING_CALL") {
            val phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER)
            val intent1 = Intent(context, DialogActivity::class.java)
            intent1.putExtra("number", phoneNumber)
            context.startActivity(intent1)
        } else if (action == "android.intent.action.PHONE_STATE") {
            val bundle = intent.extras
            val state = bundle.getString(TelephonyManager.EXTRA_STATE)
            val phoneNumber = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)

            if (state == TelephonyManager.EXTRA_STATE_RINGING) {
                val intent1 = Intent(context, DialogActivity::class.java)
                intent1.putExtra("number", phoneNumber)
                context.startActivity(intent1)
            }
        }

        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        throw UnsupportedOperationException("Not yet implemented")
    }
}
