package com.harleyquinn.part7_19

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

class NotiReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Toast.makeText(context, "I am NotiReceiver", Toast.LENGTH_SHORT).show()
    }
}
