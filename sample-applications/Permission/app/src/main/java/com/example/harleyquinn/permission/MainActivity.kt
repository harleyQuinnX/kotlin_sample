package com.example.harleyquinn.permission

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileWriter

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var contentView:EditText? = null
    var btn:Button? = null

    var fileReadPermission:Boolean = false
    var fileWritePermission:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contentView = content
        btn = savebtn
        savebtn.setOnClickListener(this)

        // 앱의 퍼미션을 자가체킹한다.
        if (ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            fileReadPermission = true
        }
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            fileWritePermission = true
        }

        // 퍼미션이 모두 허용되지 않았다면 퍼미션을 허용요청한다.
        if (!fileReadPermission || !fileWritePermission) {
            ActivityCompat.requestPermissions(this, arrayOf<String>(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE), 200)
        }

    }

    // 사용자가 앱 권한 허용 창을 닫았을 때 받는 부분
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 200 && grantResults.size > 0 ) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) fileReadPermission = true
            if (grantResults[1] == PackageManager.PERMISSION_GRANTED) fileWritePermission = true
        }
    }

    override fun onClick(p0: View?) {
        val content = contentView!!.text.toString()

        if (fileReadPermission && fileWritePermission) {
            val writer:FileWriter
            try {
                val dirPath = Environment.getExternalStorageDirectory().absolutePath + "/myApp"
                val dir = File(dirPath)

                if (!dir.exists()) {                // 디렉토리가 없으면 새로 만든다.
                    dir.mkdir()
                }

                val file = File("$dir/myfile.txt")

                if (!file.exists()) {               //  파일이 없으면 새로 만든다.
                    file.createNewFile()
                }

                writer = FileWriter(file, true)
                writer.write(content)
                writer.flush()
                writer.close()

                startActivity(Intent(this, ReadActivity::class.java))
            } catch (e:Exception) {
                e.printStackTrace()
            }
        } else {
            Toast.makeText(this, "앱 권한이 허용되지 않아 기능을 수행할 수 없습니다.", Toast.LENGTH_SHORT).show()
        }
    }
}
