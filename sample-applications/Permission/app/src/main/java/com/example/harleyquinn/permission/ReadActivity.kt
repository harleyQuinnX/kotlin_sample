package com.example.harleyquinn.permission

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import kotlinx.android.synthetic.main.activity_read_file.*
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

class ReadActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_file)

        val textView = fileResult

        val file = File(Environment.getExternalStorageDirectory().absolutePath+ "/myApp/myfile.txt");

        try {
            val reader = BufferedReader(FileReader(file))
            val buffer = StringBuffer()

            reader.forEachLine {
                var line:String = reader.readLine()
                Log.d("ReadActivity", line)
                buffer.append(line)
            }


            textView.text = buffer.toString()
            reader.close()
        } catch (e:Exception) {
            e.printStackTrace()
        }
    }
}
