package com.example.harleyquinn.permission

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.ListPreference
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.preference.PreferenceScreen

/**
 * Created by harleyquinn on 2017. 12. 6..
 * 설정 읽고 저장하는 화면 구현하기
 */
//PreferenceFragment 임!!!!
class SettingPreferenceFragment : PreferenceFragment() {

    // 화면에서 설정들을 받아옵니다.
    private val soundPreference = lazy { findPreference("sound_list") as ListPreference}
    private val keywordSoundPreference = lazy { findPreference("keyword_sound_list") as ListPreference }
    private val keywordScreen = lazy { findPreference("keyword_screen") as PreferenceScreen }

    // 모든 액티비티에서 사용할 수 있는 기본 설정파일을 읽어옵니다. 이름으로 가져오지 않았듬
    private val prefs = lazy { PreferenceManager.getDefaultSharedPreferences(activity) }

    // 이 부분을 정의하는 이유는 자동으로 sharedPreference 파일의 값은 바뀌지만 화면의 값은 반영되지 않기 때문에 반영해줍니당. 안 쓰는 파라미터 이름은 _ 이라고 지정합니다.
    private val prefListener = SharedPreferences.OnSharedPreferenceChangeListener { _: SharedPreferences, key: String ->
        if (key == "sound_list") {
            soundPreference.value.summary = prefs.value.getString("sound_list", "카톡")
        }
        if (key == "keyword_sound_list") {
            keywordSoundPreference.value.summary = prefs.value.getString("keyword_sound_list", "카톡")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.settings_preference)   // 보여줄 설정 화면 xml을 지정합니다.

        // 기본 설정파일의 기존 값들을 읽어와서 화면의 설정에 뿌려줍니다.
        if(prefs.value.getString("sound_list","") != "") {    // sound_list 설정값이 있다면 화면 설정 summary에 반영해주기
            soundPreference.value.summary = prefs.value.getString("sound_list", "카톡")
        }
        if(prefs.value.getString("keyword_sound_list", "") != "") {
            keywordSoundPreference.value.summary = prefs.value.getString("keyworld_sound_list", "카톡")
        }
        if(prefs.value.getBoolean("keyword", false)) {
            keywordScreen.value.summary = "사용"
        }

        // 값이 변경되었을 때 호출할 리스너를 붙입니다.
        prefs.value.registerOnSharedPreferenceChangeListener(prefListener)
    }


}