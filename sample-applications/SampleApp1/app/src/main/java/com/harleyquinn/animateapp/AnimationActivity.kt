package com.harleyquinn.animateapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.harleyquinn.sampleapp1.MainActivity
import com.harleyquinn.sampleapp1.R
import kotlinx.android.synthetic.main.activity_animation.*

class AnimationActivity : AppCompatActivity() {

    lateinit var imageView:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animation)

        imageView = img
        val animIn = AnimationUtils.loadAnimation(this, R.anim.`in`)

        animIn.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                val animMove = AnimationUtils.loadAnimation(this@AnimationActivity, R.anim.move)
                animMove.fillAfter = true
                imageView.animation = animMove
            }

            override fun onAnimationStart(animation: Animation?) {
                // actually, I don't need this method but I have to implement this.
            }
        })

        imageView.startAnimation(animIn)

        mainBtn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
