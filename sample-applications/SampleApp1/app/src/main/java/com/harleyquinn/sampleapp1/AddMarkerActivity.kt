package com.harleyquinn.sampleapp1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.harleyquinn.sampleapp1.fragment.MapForAddMarkerFragment
import kotlinx.android.synthetic.main.activity_add_marker_acitivy.*

class AddMarkerActivity : AppCompatActivity() {

    companion object {
        val REQUEST_SUCCESS = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_marker_acitivy)

        supportFragmentManager
                .beginTransaction()
                .add(R.id.mapLayout, MapForAddMarkerFragment(), "naverMap")
                .commit()

        saveBtn.setOnClickListener {
            val intent = Intent()
            intent.putExtra("point", MapForAddMarkerFragment.itemPoint)

            setResult(REQUEST_SUCCESS, intent)
            finish()
        }
    }
}
