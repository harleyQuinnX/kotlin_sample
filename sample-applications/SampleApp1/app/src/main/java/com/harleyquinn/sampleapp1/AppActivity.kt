package com.harleyquinn.sampleapp1

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity

// 그냥 맨 처음에 시작할 때 있는 app이에요
// 제가 제 이름 적어놨는데 너무 빨리 뜨고 다음 인텐트 넘어가서 안보임 흐규
class AppActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)

        startActivity(Intent(this, MainActivity::class.java))
    }
}
