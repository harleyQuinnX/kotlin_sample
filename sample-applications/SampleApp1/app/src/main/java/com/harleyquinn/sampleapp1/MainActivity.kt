package com.harleyquinn.sampleapp1

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import com.harleyquinn.sampleapp1.fragment.MapFragment
import com.harleyquinn.sampleapp1.fragment.MenuFragment
import com.nhn.android.maps.maplib.NGeoPoint

class MainActivity : FragmentActivity() {

    private val mapFragment = MapFragment()
    private var count = 0

    companion object {
        private val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
                .beginTransaction()
                .add(R.id.menuLayout, MenuFragment(), "menu")
                .add(R.id.mapLayout, mapFragment, "naverMap")
                .commit()
    }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(resultCode) {
            AddMarkerActivity.REQUEST_SUCCESS -> {
                val point = data?.extras?.get("point") as NGeoPoint
                mapFragment.makeMarker(point, "새로 찍음 ${count++}")
                Log.d(TAG, "MainActivity")
            }

        }
    }

}
