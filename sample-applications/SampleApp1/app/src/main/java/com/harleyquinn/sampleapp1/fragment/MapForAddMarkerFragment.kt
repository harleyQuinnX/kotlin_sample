package com.harleyquinn.sampleapp1.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.harleyquinn.sampleapp1.App
import com.harleyquinn.sampleapp1.R
import com.harleyquinn.sampleapp1.forMap.NMapPOIflagType
import com.harleyquinn.sampleapp1.forMap.NMapViewerResourceProvider
import com.nhn.android.maps.NMapContext
import com.nhn.android.maps.NMapView
import com.nhn.android.maps.maplib.NGeoPoint
import com.nhn.android.maps.overlay.NMapPOIdata
import com.nhn.android.maps.overlay.NMapPOIitem
import com.nhn.android.mapviewer.overlay.NMapOverlayManager
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay
import kotlinx.android.synthetic.main.fm_map.*

class MapForAddMarkerFragment : Fragment(), NMapPOIdataOverlay.OnFloatingItemChangeListener {

    private lateinit var mMapContext : NMapContext
    private lateinit var mMapViewerResourceProvider : NMapViewerResourceProvider
    private lateinit var mMapOverlayManager : NMapOverlayManager

    companion object {
        lateinit var itemPoint : NGeoPoint
        private lateinit var mMapView : NMapView

    }

    override fun onPointChanged(p0: NMapPOIdataOverlay?, p1: NMapPOIitem) {
        itemPoint = p1.point
        Log.i("MapForAddMarkerFragment", "onPointChanged: point=" + itemPoint.toString())
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater?.inflate(R.layout.fm_map, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mMapContext = NMapContext(super.getActivity())
        mMapContext.onCreate()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mMapView = mapView
        mMapView.setClientId(App.clientId)
        mMapView.isClickable = true
        mMapView.isFocusable = true
        mMapView.isFocusableInTouchMode = true
        mMapView.isEnabled = true
        mMapView.requestFocus()
        mMapContext.setupMapView(mMapView)

        mMapViewerResourceProvider = NMapViewerResourceProvider(super.getActivity())
        mMapOverlayManager = NMapOverlayManager(super.getActivity(), mMapView, mMapViewerResourceProvider)

        val mPoiData = NMapPOIdata(2, mMapViewerResourceProvider)

        mPoiData.beginPOIdata(1)
        val item : NMapPOIitem = mPoiData.addPOIitem(null, "Touch & Drag to Move", NMapPOIflagType.PIN, 0)
        item.point = mMapView.mapController.mapCenter
        item.floatingMode = NMapPOIitem.FLOATING_TOUCH or NMapPOIitem.FLOATING_DRAG
        mPoiData.endPOIdata()

        val mPoiDataoverlay = mMapOverlayManager.createPOIdataOverlay(mPoiData, null)
        mPoiDataoverlay.setOnFloatingItemChangeListener(this)

        itemPoint = mMapView.mapController.mapCenter

        }

    override fun onStart() {
        super.onStart()
        mMapContext.onStart()
    }

    override fun onResume() {
        super.onResume()
        mMapContext.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapContext.onPause()
    }

    override fun onStop() {
        mMapContext.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        mMapContext.onDestroy()
        super.onDestroy()
    }


}