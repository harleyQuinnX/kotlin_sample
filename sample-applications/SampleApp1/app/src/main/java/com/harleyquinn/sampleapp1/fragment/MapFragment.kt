package com.harleyquinn.sampleapp1.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.harleyquinn.sampleapp1.App
import com.harleyquinn.sampleapp1.R
import com.harleyquinn.sampleapp1.forMap.NMapPOIflagType
import com.harleyquinn.sampleapp1.forMap.NMapViewerResourceProvider
import com.nhn.android.maps.NMapContext
import com.nhn.android.maps.NMapView
import com.nhn.android.maps.maplib.NGeoPoint
import com.nhn.android.maps.overlay.NMapPOIdata
import com.nhn.android.mapviewer.overlay.NMapOverlayManager
import kotlinx.android.synthetic.main.fm_map.*
import org.naver.models.map.Location

class MapFragment : Fragment() {
    private lateinit var mMapContext : NMapContext
    private lateinit var mMapViewerResourceProvider : NMapViewerResourceProvider
    private lateinit var mMapOverlayManager : NMapOverlayManager

        init {
        instance = this
    }
    companion object {
        lateinit var mMapView : NMapView
        lateinit var NMapPOIdataOverlay : NMapPOIdata
        lateinit var instance:MapFragment
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater?.inflate(R.layout.fm_map, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mMapContext = NMapContext(super.getActivity())
        mMapContext.onCreate()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mMapView = mapView
        mMapView.setClientId(App.clientId)
        mMapView.isClickable = true
        mMapView.isFocusable = true
        mMapView.isFocusableInTouchMode = true
        mMapView.isEnabled = true
        mMapView.requestFocus()
        mMapContext.setupMapView(mMapView)

        mMapViewerResourceProvider = NMapViewerResourceProvider(super.getActivity())

        mMapOverlayManager = NMapOverlayManager(super.getActivity(), mMapView, mMapViewerResourceProvider)
        NMapPOIdataOverlay = NMapPOIdata(2, mMapViewerResourceProvider)
    }

    fun makeMarker(point:NGeoPoint, addr:String) {
        val poiData = NMapPOIdata(1, mMapViewerResourceProvider)
        poiData.beginPOIdata(1)
        poiData.addPOIitem(point, addr, NMapPOIflagType.PIN, 0)
        poiData.endPOIdata()

        val poiDataOverlay = mMapOverlayManager.createPOIdataOverlay(poiData, null)
        poiDataOverlay.showAllPOIdata(0)

    }
    fun moveMap(location: Location) {
        val point = NGeoPoint(location.point.x, location.point.y)
        mMapView.mapController.mapCenter = point
        makeMarker(point, location.address)
    }

    override fun onStart() {
        super.onStart()
        mMapContext.onStart()
    }

    override fun onResume() {
        super.onResume()
        mMapContext.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapContext.onPause()
    }

    override fun onStop() {
        mMapContext.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        mMapContext.onDestroy()
        super.onDestroy()
    }
}