package com.harleyquinn.sampleapp1.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.harleyquinn.sampleapp1.AddMarkerActivity
import com.harleyquinn.sampleapp1.R
import com.harleyquinn.sampleapp1.network.GeocodeTask
import com.harleyquinn.sampleapp1.toast
import com.nhn.android.maps.maplib.NGeoPoint
import kotlinx.android.synthetic.main.fm_menu.*

class MenuFragment : Fragment() {

    companion object {
        val REQUEST_ADD_MARKER = 0
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater?.inflate(R.layout.fm_menu, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        searchBtn.setOnClickListener {
            val addr = addrEdit.text.toString()
            toast(activity, addr)
            Log.d(tag, addr)

            GeocodeTask().execute(addr)
        }

        addMarkerBtn.setOnClickListener {
            startActivityForResult(Intent(activity, AddMarkerActivity::class.java), REQUEST_ADD_MARKER)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(resultCode) {
            AddMarkerActivity.REQUEST_SUCCESS -> {
                val point = data?.extras?.get("point") as NGeoPoint
                Log.d(tag, point.toString()+"여긴 매뉴우우우 ")
            }

        }
    }

}