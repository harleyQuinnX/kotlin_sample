package com.harleyquinn.sampleapp1.network

import android.os.AsyncTask
import android.util.Log
import com.harleyquinn.sampleapp1.App
import com.harleyquinn.sampleapp1.fragment.MapFragment
import org.naver.models.map.Location

class GeocodeTask : AsyncTask<String, String, String>() {

    private lateinit var location : Location

    companion object {
        val tag : String = "GeocodeTask"
    }

    override fun onPreExecute() {
        super.onPreExecute()
        Log.d(tag, "AsynkTask onPreExecute")
    }

    override fun doInBackground(vararg params: String?): String {

        // 한국어 입력이 안되서 그냥 값을 줘놨습니다.
//            val addr = params[0].toString()
        val addr = "불정로 6"

        try {
            val locations = App.naver.map().geocode(addr)
            Log.d(tag, locations.toString())
            locations.items.forEach { location ->
                Log.d(tag, location.point.x.toString() + " , " + location.point.y.toString())
            }
            Log.d(tag, "검색결과 : ${locations.items.size} 건이 검색되었습니다.")

            // 1. 첫번째 주소 좌표를 기준으로 맵을 이동시킨다.
            location = locations.items.first()

            // 2. 주소 결과값들을 나열해준다.
            return "success"

        } catch (Ex : Exception) {
            Log.d(tag, "Error in doInBackground " + Ex.message )
            return "error"
        }
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        if(result == "success") {
            MapFragment.instance.moveMap(location)
        }
    }
}