package com.harleyquinn.simplegithub

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class RepositoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository)
    }

    companion object {
        val KEY_USER_LOGIN = ""
        val KEY_REPO_NAME = ""
    }
}
