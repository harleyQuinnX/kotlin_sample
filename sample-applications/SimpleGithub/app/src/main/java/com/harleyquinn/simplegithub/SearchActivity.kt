package com.harleyquinn.simplegithub

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_search.*
import org.w3c.dom.Text

class SearchActivity : AppCompatActivity(), SearchAdapter.ItemClickListener {

    lateinit var api:GithubApi
    lateinit var searchView:SearchView
    lateinit var menuSearch:MenuItem
    lateinit var adapter:SearchAdapter
    lateinit var progress:ProgressBar
    lateinit var tvMessage:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val rvList = rvActivitySearchList
        tvMessage = tvActivitySearchMessage
        progress = pbActivitySearch

        val adapter = SearchAdapter()
        adapter.itemClickListener = this

        rvList.layoutManager = LinearLayoutManager(this)
        rvList.adapter = adapter

        api = GithubApiProvider.provideGithubApi(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu_activity_search, menu)
        menuSearch = menu.findItem(R.id.menu_activity_search_query)

        searchView = menuSearch.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                updateTitle(query)
                hideSoftKeyboard()
                collapseSearchView()
                searchRepository(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean = false

        })

        menuSearch.expandActionView()

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (R.id.menu_activity_search_query == item.itemId) {
            item.expandActionView()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onItemClick(repository:GithubRepo) {
        Intent(this, RepositoryActivity::class.java).let {
            it.putExtra(RepositoryActivity.KEY_USER_LOGIN, repository.owner.login)
            it.putExtra(RepositoryActivity.KEY_REPO_NAME, repository.name)
            startActivity(it)
        }

    }

    private fun searchRepository(query:String) {
        clearResults()
        hideError()
        showProgress()

        val searchCall = api.searchRepository(query)
        searchCall.enqueue(Callback<RepoSearchResponse>() {

        })
    }

    private fun updateTitle(query:String) {
        val ab = supportActionBar
        if (ab != null) {
            ab.subtitle = query
        }
    }

    private fun hideSoftKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(searchView.windowToken, 0)

    }

    private fun collapseSearchView() {
        menuSearch.collapseActionView()
    }

    private fun clearResults() {
        adapter.clearItems()
        adapter.notifyDataSetChanged()
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progress.visibility = View.GONE
    }

    private fun showError(message:String) {
        tvMessage.text = message
        tvMessage.visibility = View.VISIBLE
    }

    private fun hideError() {
        tvMessage.text = ""
        tvMessage.visibility = View.GONE
    }
}
