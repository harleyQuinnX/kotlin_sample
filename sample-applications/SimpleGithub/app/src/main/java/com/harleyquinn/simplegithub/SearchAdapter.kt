package com.harleyquinn.simplegithub

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by harleyquinn on 2018. 1. 4..
 */
class SearchAdapter : RecyclerView.Adapter<SearchAdapter.RepositoryHolder>() {

    var items = arrayListOf<GithubRepo>()

    private val placeholder = ColorDrawable(Color.GRAY)
    private var itemClickListener:ItemClickListener? = null

    override fun onBindViewHolder(holder: RepositoryHolder, position: Int) {
        val repo = items[position] as GithubRepo

        GlideApp.with(holder.itemView.context)
                .load(repo.owner.avatarUrl)
                .placaholder(placeholder)
                .into(holder.ivProfile)

        holder.tvName.text = repo.fullName
        holder.tvLanguage.text = if(TextUtils.isEmpty(repo.language)) holder.itemView.context.getText(R.string.no_language_specified) else repo.language

        holder.itemView.setOnClickListener {
            if (itemClickListener != null) {
                itemClickListener!!.onItemClick(repo)
            }
        }

    }

    override fun getItemCount(): Int = items.size
    fun clearItems() = this.items.clear()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryHolder  = RepositoryHolder(parent)

    interface ItemClickListener {

        fun onItemClick(repository:GithubRepo)
    }

    class RepositoryHolder(parent:ViewGroup) : RecyclerView.ViewHolder((LayoutInflater.from(parent.getContext())).inflate(R.layout.item_repository, parent, false)) {

        val ivProfile:ImageView = itemView.findViewById(R.id.ivItemRepositoryProfile)
        val tvName:TextView = itemView.findViewById(R.id.tvItemRepositoryName)
        val tvLanguage:TextView = itemView.findViewById(R.id.tvItemRepositoryLanguage)

    }
}