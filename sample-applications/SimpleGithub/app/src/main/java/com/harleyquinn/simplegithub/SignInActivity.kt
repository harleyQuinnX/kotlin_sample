package com.harleyquinn.simplegithub

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {

    private lateinit var api:AuthApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        btnActivitySignInStart.setOnClickListener {
            // 사용자 인증 처리 url 구성

            val authUri = Uri.Builder().scheme("https")
                    .authority("github.com")
                    .appendPath("login")
                    .appendPath("oauth")
                    .appendPath("authorize")
                    .appendQueryParameter("client_id", BuildConfig.GITHUB_CLIENT_ID)
                    .build()

            CustomTabsIntent.Builder().build().launchUrl(this@SignInActivity, authUri)
        }

        api = GithubApiProvider.provideAuthApi()
        val authTokenProvider = AuthTokenProvider(this)

        if (authTokenProvider.getToken != null) {
            launchMainActivity()
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        showProgress()

        val uri = intent.data ?: throw IllegalAccessException("No data exists")
        val code = uri.getQueryParameter("code") ?: throw IllegalStateException("No code exists")

        getAccessToken(code)
    }

    private fun getAccessToken(code:String) {
        showProgress()

        // 액세스 토큰을 요청하는 REST API
        val accessTokenCall = api.getAccessToken(BuildConfig.GITHUB_CLIENT_ID, BuildConfig.GITHUB_CLIENT_SECRET, code)

        // 비동기 방식으로 액세스 토큰 요청
        accessTokenCall.enqueue(object :Callback<GithubAccessToken>() {
            override fun onResponse(call:Call<GithubAccessToken>, response:Response<GithubAcessToken>) {
                hideProcess()

                val githubAccessToken = response.body()

                if(response.isSuccessful() && token!=null) {
                    authTokenProvider.updateToken(token.accessToken)

                    launchMainActivity()
                } else {
                    showError(IllegalStateException("Not successful : ${response.message()}"))
                }
            }

            override fun onFailure(Call<GithubAccessToken>)
        })
    }
}
