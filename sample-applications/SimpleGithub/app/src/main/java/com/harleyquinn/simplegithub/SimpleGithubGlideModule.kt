package com.harleyquinn.simplegithub

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by harleyquinn on 2018. 1. 4..
 */
@GlideModule
class SimpleGithubGlideModule : AppGlideModule() {
}